import Vue from 'vue'
import Router from 'vue-router'
import MainPage from "../components/MainPage";
import HelloWorld from "../components/HelloWorld";
import products from "./products";
import auth from "./auth";
import test from '../components/test';
import test1 from '../components/test/test1'
import Registr from '../components/test/Registr';

Vue.use(Router)

export default new Router({
    mode: "history",
    routes: [
        {path: '/main', name: 'MainPage',component: MainPage},
        {path: '/hello', name: 'HelloWorld', component: HelloWorld},
        {path: '/test', name: 'Test', component: test},
        {path: '/registr', name: 'Registr', component: Registr},
        ...products,
        ...auth

    ]
})
