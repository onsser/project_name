import Products from "../components/products/Index"
import Create from "../components/products/Create";
import Product from "../components/products/Product";
import Edit from "../components/products/Edit";

export default [
    { path: '/products', name: 'Products', component: Products },
    { path: '/products/create', name: 'Products-Create', component: Create },
    { path: '/products/:id', name: 'Products-Show', component: Product },
    { path: '/products/:id/edit', name: 'Products-Edit', component: Edit },
]
