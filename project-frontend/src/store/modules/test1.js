import axios from  "axios";

export  default {
    state: {
        num: 0
    },
    getters: {
        getNumber(state) {
            return state.num
        }
    },
    mutations: {
        changeNumber(state, data) {
            return state.num = data
        },

    }
}
