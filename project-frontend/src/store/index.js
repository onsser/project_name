import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import auth from "./modules/auth";
import test1 from "./modules/test1";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        products: []
    },
    mutation: {},
    getters: {
        products(state) {
            return state.products
        }
    },
    actions: {
        getProducts(context) {
            axios.get('/products').then(res => {
                context.commit('storeProducts', res.data.data)
            })
        }
    },
    modules: {
        auth: auth,
        test1:test1
    }

})
